from random import randint

name = input("Hi! What is your name?")


#num_guesses = int(5)

guess_number = 1


for guess_num in range(1 , 6):
    print("Guess", str(guess_number), ":", name, "were you born in", randint(1,12),"/", randint(1924,2004), "?" )
    guess = input ("yes or no?")
    guess_number = guess_number + 1


    if guess == "yes":
        print("I knew it!")
        exit()
    elif guess == "no":
        print("Drat! Let me try again!")
    else:
        print("I have other things to do. Good Bye.")
